import React  from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Image ,Button,Dimensions,TextInput} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {connect} from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome5';


const DEVICE = Dimensions.get('window')
const jum = 12;

const Item = ({titless}) => (
    <View style={styles.setflex}>
        <View style={styles.item}>
            <View style={styles.item2}>
                <Image
                    style={styles.tinyLogo}
                    source={{
                        uri: 'https://reactnative.dev/img/tiny_logo.png',
                    }}
                />
                <Text style={styles.titleitem2}>{titless}</Text>
            </View>
        </View>
    </View>
);
  
class ListItem extends React.Component {
    currencyFormat(num) {
      return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
  
    DetailGambar (data){
        //alert('klik gambar');
        }
    render() {
     
     const data = this.props.databarang
      return (
        <View style={styles.itemContainer}>
            <TouchableOpacity onPress={this.DetailGambar.bind(this)}>
                <Image source={{ uri: data.url }} style={styles.itemImage} resizeMode='contain'
            />
            </TouchableOpacity>
          
          <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.nama}</Text>
          <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.harga))}</Text>
          <Text style={styles.itemStock}>Sisa stok: {data.stok}</Text>
          <TouchableOpacity title='BELI' 
            style={{width:hp('10%'),backgroundColor:'blue',borderWidth:0,alignItems:'center',borderRadius:hp('1%')}}
          onPress={() => {
            this.props.updatePrice(data)
          }}>
            <Text style={{color:'white',fontSize:hp('3%')}}>BELI</Text>

          </TouchableOpacity>
        </View>
      )
    }
  };

class Home extends React.Component {
   componentWillMount() {
        this.props.BarangApi()
    }

    currencyFormat(num) {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
      };

    updatePrice(price) {
        console.log('ini pride ',price)
        this.props.setharga(price.harga+this.props.totalbeli)
   
    }

    caridata(str) {
        let dc=(str.searchText).toUpperCase();
        let dsa=this.props.arraysearchbarang;
      //  console.log(dsa)
      let arrbr=[]
        for(let p=0;p<dsa.length;p++){
            let std=(dsa[p].nama).toUpperCase();
            let n = std.includes(dc);
            if(n){
                 let bar={
                    Deskripsi:dsa[p].Deskripsi,
                    harga:dsa[p].harga,
                    nama:dsa[p].nama,
                    stok:dsa[p].stok,
                    tipe:dsa[p].tipe,
                    url:dsa[p].url,
                    id:dsa[p].id
                  }
                  arrbr.push(bar);
            }
        }
        this.props.CariBarangState(arrbr);

        //console.log('sakmad ',dc);
        //let n = str.includes("world");
    }

   

    render() {
        const renderItem = ({ item }) => (
            <Item titless={item.nama} />
        );
       return (
             
           
            <View style={styles.container}>
                <TouchableOpacity style={{left:wp('42%'), top:wp('4%') }}
                    onPress={() => {
                        this.props.KembaliHome()
                        }}
                    >
                      <Icon name='sign-out-alt' size={30} color='red' />
                </TouchableOpacity>
                <View style={{color:'red' ,paddingTop:hp('2%')}}>
                        
                </View>
                <View style={styles.header}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{fontWeight: 'bold'}}>Hai,{'\n'}
                        {this.props.namauser}
                       
                        </Text>
                        {/* <Text style={styles.headerText}>Daftar Barang</Text> */}
                     
                        <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
                        <Text style={styles.headerText}>
                             {this.currencyFormat(this.props.totalbeli)} 
                            </Text>
                        </Text>
                    </View>
                    <View>

                    </View>
                    <TextInput
                        style={{ backgroundColor: 'white', marginTop: 8 }}
                        placeholder='Cari barang..'
                        onChangeText={(searchText => this.caridata({ searchText }))}
                    />
                </View>
                <FlatList 
                    data={this.props.arraybarang}
                    renderItem={(databarang)=> <ListItem updatePrice={this.updatePrice.bind(this)} databarang={databarang.item} />}
                    keyExtractor={(skill)=> (skill.id+'')}
                    numColumns={2}
                    ItemSeparatorComponent={()=>
                    <View style={{height:0.4,backgroundColor:'#E5E5E5',borderColor:'red',borderWidth:0}}/>}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop:hp('1%'),
        justifyContent: 'center',
        alignItems: 'center',
                
      },
      header:{
        minHeight:hp('3%'),
        width:wp('95%'),
        minHeight:hp('5%')
      },
      headerText: {
        fontSize: 18,
        fontWeight: 'bold'
      },
    
      //? Lanjutkan styling di sini
      itemContainer: {
        width: DEVICE.width * 0.47,
        //flexDirection:'row',
        //borderColor:'blue',
        //borderWidth:3,
        height:200,
        //flexWrap:'wrap',
        alignItems:'center',
        justifyContent:'center',
        padding:4,
        borderWidth:4,
        borderColor:'#E5E5E5',
        backgroundColor:'white',
       // borderColor:'white'
        
      },
      itemImage: {
        marginTop:5,
        flexDirection:'row',
        width:150,
        height:70,
        flexWrap:'wrap',
        justifyContent:'center',
        borderWidth:0,
        
        
      },
      itemName: {
        fontSize:14,
        fontWeight:'bold',
        justifyContent:'center',
        
        
      },
      itemPrice: {
        color:'blue',
        justifyContent:'center',
        
      },
      itemStock: {
        justifyContent:'center',
        flexDirection:'row',
      },
      itemButton: {
        justifyContent:'center'
      },
      buttonText: {
      }
});

const CekabarangApi = (data) => (dispatch) =>  {
   // console.log('ini dispatch ',data)
    //console.log('ini props  ',this.date)
    let url=''
    if(data!=null){
        url='https://azd25-cc8c3.firebaseio.com/DB/barang/nama'+data+'.json';
    }else{
        url='https://azd25-cc8c3.firebaseio.com/DB/barang.json';
    }
   
    fetch(url)
          .then(res => res.json())
          .then(res => {
              if(res.error) {
                  throw(res.error);
              }
              //console.log('ininreponyaaaaaaaaaaaaaaaaaaaaa ',res)
              if(res==null){
                alert(' User Tidak Ditemukan ');
                dispatch({type:'Set_Barang',value:""});
              }else {
                let arrbr=[]
                  for(let p=0;p<res.length;p++){
                      if(res[p]!=null){
                          let bar={
                            Deskripsi:res[p].Deskripsi,
                            harga:res[p].harga,
                            nama:res[p].nama,
                            stok:res[p].stok,
                            tipe:res[p].tipe,
                            url:res[p].url,
                            id:p
                          }
                          arrbr.push(bar);
                      }
                      //console.log('dataaaaaaaa',res[p])
                  }
                dispatch({type:'Set_Barang',value:arrbr});
              }
                        
              //return res.products;
          })
          .catch(error => {
            alert(' User Tidak Ditemukan ');
            dispatch({type:'Set_Barang',value:""});
          })
  }

const mapStateToProp=(state)=>{
    //console.log("imapStateToProp home ",state.arraybarang);
    return {
        arraybarang:state.arraybarang,
        namauser:state.namauser,
        totalbeli:state.totalbeli,
        arraysearchbarang:state.arraysearchbarang
    }

}

const mapDispatchToProp=(dispatch)=>{
    return{
        BarangApi:(a) => dispatch(CekabarangApi(a)),
        setharga:(a) => dispatch({type:'Set_Harga',value:a}),
        CariBarangState:(a) => dispatch({type:'Cari_Barang',value:a}),
        KembaliHome:(a)=> dispatch({type:'Kembali_Home'}),
    }
  
  }


export default connect(mapStateToProp,mapDispatchToProp) (Home);