import React  from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image,TextInput } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {connect} from 'react-redux';

class About extends React.Component {
    constructor(props) {
        super(props);
               
    }
    
    render() {
        // alert("===> "+this.props.user);
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity style={{left:wp('90%'), top:wp('12%') }}
                        onPress={() => {
                            this.props.KembaliLogin()
                            }}
                        >
                        <Icon name='sign-out-alt' size={30} color='red' />
                    </TouchableOpacity>
                    <Text style={styles.tittle}>About</Text>
                </View>
                <View style={styles.footer}>
                    <View style={styles.setLogo}>
                        <Image
                            style={styles.tinyLogo}
                            source={{
                            uri: this.props.image,
                            }}
                        />
                        <Text style={styles.txdata}>{this.props.user}</Text>
                        <Text style={styles.txdata}>{this.props.email}</Text>
                        <Text style={styles.grs}>-------------------------------------------------------------------</Text>
                        <Text style={styles.own}>Owner</Text>
                        <Text style={styles.own}>GageNgembil.com</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        flex: 1,
        backgroundColor: '#0C50FF',
        borderBottomLeftRadius:20,
        borderBottomRightRadius:20
    },
    footer: {
        flex: 7,
        backgroundColor: '#fff',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20
    },
    tittle: {
        fontSize: 20,
        color: '#fff',
        textAlign: 'center',
        top : 20
    },
    tinyLogo: {
        height:hp('20%'),
        width:wp('30%'),
        borderRadius: 100
    },
    setLogo: {
        alignItems: 'center',
        top: hp('1%')
    },
    txdata: {
        top: hp('1%'),
        fontSize: hp('2%'),
        color: '#000'
    },
    dtdetail: {
        alignItems: 'center',
    },
    textinput: {
        top: hp('3%'),
        height: hp('5%'),
        width: wp('85%'),
        borderWidth: 1,
        paddingHorizontal: wp('5%'),
        borderRadius: wp('2%'),
        backgroundColor: '#fff'
    },
    tengah: {
        alignItems: 'center'
    },
    appButtonContainer: {
        backgroundColor: "red",
        borderRadius: hp('5%'),
        paddingVertical: hp('2%'),
        width: wp('40%'),
        justifyContent: 'center',
        alignItems: 'center',
        top: wp('10%'),
    },
    appButtonText: {
        fontSize: hp('2%'),
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center"
    },
    grs: {
        fontSize: hp('2%'),
        fontWeight: "bold",
        alignSelf: "center",
        top: hp('3%')
    },
    own: {
        fontSize: hp('4%'),
        textAlign: 'center',
        top : hp('5%'),
        color: '#0C50FF'
    },
});


const mapStateToProp=(state)=>{
    return {
        user:state.namauser,
        password:state.pasword,
        login:state.login,
        loading:state.loading,
        image:state.user.image,
        email:state.user.email,
        tlahir:state.user.tempatlahir,
        tgllahir:state.user.tangallahir,
        jk:state.user.jeniskelamin,
        juser:state.user.jenisuser,
        handp:state.user.hp,
    }
  
}

const mapDispatchToProp=(dispatch)=>{
    return{
        Setuser:(a)=> dispatch({type:'Setuser',namauser:a}),
        Setpassword:(a)=> dispatch({type:'Setpassword',password:a}),
        setloading:() => dispatch({type:'Update_loading'}),
        UserApi:(a) => dispatch(CekUserApi(a)),
        KembaliLogin:(a)=> dispatch({type:'Kembali_Home'}),
    }
  
}

export default connect(mapStateToProp,mapDispatchToProp)(About);