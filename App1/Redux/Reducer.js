import React, { Component } from 'react'


const inisialState={
    namauser:"ahmad",
    pasword:"A",
    loading:false,
    login:false,
    user:'',
    totalbeli:0,
    arraybarang:[],
    arraybarangsetting:[],
    arraysearchbarang:[],
    namabar:"",
    hargabar:0,
    srokbar:0
  }



const indukreducer=(state=inisialState,action) => {
    if(action.type==="Setuser"){
        console.log("set user ",action.namauser);
        return {
            ...state,
            namauser:action.namauser
        }
    }

    if(action.type==="Setpassword"){
        console.log("set user ",action.password);
        return {
            ...state,
            pasword:action.password
        }
    }


    if(action.type==="Update_User"){
        return {
            ...state,
            user:action.value,
            login:action.respon,
            loading:false
        }
    }
  
    if(action.type==="Update_loading"){
        return {
            ...state,
            loading:true
        }
        }

    if(action.type==="Set_Barang"){
        return {
            ...state,
            arraybarang:action.value,
            arraysearchbarang:action.value,
            arraybarangsetting:action.value,
            loading:true
        }
        }
    if(action.type==="Kembali_Home"){
        return {
            ...state,
            login:false,
            loading:false
        }
        }

    if(action.type==="Set_Harga"){
        return {
            ...state,
            totalbeli:action.value,
        }
        }

    if(action.type==="Cari_Barang"){
        return {
            ...state,
            arraybarang:action.value,
        }
        }

    if(action.type==="Set_Namabar"){
        return {
            ...state,
            namabar:action.value,
        }
        }
    if(action.type==="Set_Hargabar"){
        return {
            ...state,
            hargabar:action.value,
        }
        }

    if(action.type==="Set_Stokbar"){
        console.log('ini sction so stol ',action);
        return {
            ...state,
            srokbar:action.value,
        }
        }

    if(action.type==="Set_Lisebarang"){
        return {
            ...state,
            arraybarang:action.value,
            arraysearchbarang:action.value,
            arraybarangsetting:action.value,
            namabar:"",
            hargabar:0,
            srokbar:0,
        }
        }

    if(action.type==="Cari_BarangSetiing"){
        return {
            ...state,
            arraybarangsetting:action.value,
        }
        }

    return state;
  
  }

  export default indukreducer;