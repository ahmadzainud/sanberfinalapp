import React  from 'react';
import { View, Text, StyleSheet, TouchableOpacity,TextInput,FlatList,Image } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {connect} from 'react-redux'

class ListItemsetting extends React.Component {
        
    currencyFormat(num) {
      return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
  
    DetailGambar (data){
       // alert('klik gambar');
        }

    
    SetStok (data){
       this.props.getprop.SetStokbar( data.searchText );
    }
    SetHarga (data){
        this.props.getprop.SetHargabar( data.searchText );
    }

    SetNama (data){
     this.props.getprop.SetNAmabar( data.searchText );
    
    }


    render() {
        
     const data = this.props.databarang
      return (
       
          <View style={{borderWidth:4,flex:1,borderColor:'#E5E5E5',flexDirection:'row'}}>
              <View style={{borderWidth:0,flex:1,alignItems:"center"}}>
              <TouchableOpacity onPress={this.DetailGambar.bind(this)}
                 >
                    <Image source={{ uri: data.url }} 
                     style={{width:hp('25%'),
                     height:hp('25%'),
                     marginRight:hp('1%')
                    }} 
                    />
                </TouchableOpacity>
                  
              </View>
              <View style={{flex:1,flexDirection:'column'}}>
                <View style={{borderWidth:0,flexDirection:'row',margin:2}} >
                    <View style={{borderWidth:0,flex:1,justifyContent:'center'}}>
                            <Text styles={{borderWidth:0}}>Nama</Text>
                    </View>
                    <View style={{borderWidth:0,flex:3}}>
                    <TextInput  style={{
                        borderWidth:0,
                        width:wp('35%'),
                        backgroundColor:'white'
                        }}
                        defaultValue={data.nama}
                        placeholder='Nama Barang'
                        onChangeText={(searchText => this.SetNama({ searchText }))}
                        />
                    </View>      
                </View>
                <View style={{borderWidth:0,flexDirection:'row',margin:2}} >
                    <View style={{borderWidth:0,flex:1,justifyContent:'center'}}>
                            <Text styles={{borderWidth:0}}>Harga</Text>
                    </View>
                    <View style={{borderWidth:0,flex:3}}>
                    <TextInput  style={{
                        borderWidth:0,
                        width:wp('35%'),
                        backgroundColor:'white'
                        }}
                        defaultValue={data.harga+''}
                        onChangeText={(searchText => this.SetHarga({ searchText }))}
                        placeholder='Harga'/>
                    </View>
                 </View>
                 <View style={{borderWidth:0,flexDirection:'row',margin:2}} >
                    <View style={{borderWidth:0,flex:1,justifyContent:'center'}}>
                            <Text styles={{borderWidth:0}}>Stok</Text>
                    </View>
                    <View style={{borderWidth:0,flex:3}}>
                    <TextInput  style={{
                        borderWidth:0,
                        width:wp('35%'),
                        backgroundColor:'white'
                        }}
                        defaultValue={data.stok+''}
                        onChangeText={(searchText => this.SetStok({ searchText }))}
                        placeholder='Stok'/>
                    </View>
                 </View>
                 <View style={{borderWidth:0,margin:hp('1%'),alignItems:'center',marginTop:wp('5%')}} >
                    <TouchableOpacity title='UPDATE' 
                            style={{width:hp('20%'),backgroundColor:'green',borderWidth:0,alignItems:'center',borderRadius:hp('1%')}}
                        onPress={() => {
                            this.props.updateBarang(data)
                        }}
                        
                        >
                        <Text style={{color:'white',fontSize:hp('3%')}}>UPDATE</Text>
                    </TouchableOpacity> 
                 </View>
              </View>
   


          </View>
       
      )
    }
  };

 class Setting extends React.Component {
     
    
    currencyFormat(num) {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
      };

    

    updateBarang(price) {
        console.log('ini pride =======================',price)
        let arrbr=[]
        let dsa=this.props.arraybarang;
        for(let p=0;p<dsa.length;p++){
            let bar={}
            
            if(price.id===dsa[p].id){
                let namatrik='';
                if(this.props.namabar!=null && this.props.namabar !=''){
                    namatrik=this.props.namabar;
                }else{
                    namatrik=price.nama;
                }

                let hargatrik='';
                if(this.props.hargabar!=null && this.props.hargabar !=0){
                    hargatrik=this.props.hargabar;
                }else{
                    hargatrik=price.harga;
                }

                let stoktrik='';
                if(this.props.srokbar!=null && this.props.srokbar !=0){
                    stoktrik=this.props.srokbar;
                }else{
                    stoktrik=price.stok;
                }

                 bar={
                    Deskripsi:dsa[p].Deskripsi,
                    harga:hargatrik,
                    nama:namatrik,
                    stok:stoktrik,
                    tipe:dsa[p].tipe,
                    url:dsa[p].url,
                    id:dsa[p].id
                  }
            }else{
                bar={
                    Deskripsi:dsa[p].Deskripsi,
                    harga:dsa[p].harga,
                    nama:dsa[p].nama,
                    stok:dsa[p].stok,
                    tipe:dsa[p].tipe,
                    url:dsa[p].url,
                    id:dsa[p].id
                  }

            }
            arrbr.push(bar);
        }
          this.props.SetLiseBarang(arrbr);
          alert('Data Berhasil Dirubah ');
    }

    masuk(str) {
       // console.log('ini pride ',this.props)
        let dc=(str.searchText).toUpperCase();
        let dsa=this.props.arraycari;
      
      let arrbr=[]
        for(let p=0;p<dsa.length;p++){
            let std=(dsa[p].nama).toUpperCase();
            let n = std.includes(dc);
            if(n){
                 let bar={
                    Deskripsi:dsa[p].Deskripsi,
                    harga:dsa[p].harga,
                    nama:dsa[p].nama,
                    stok:dsa[p].stok,
                    tipe:dsa[p].tipe,
                    url:dsa[p].url,
                    id:dsa[p].id
                  }
                  arrbr.push(bar);
            }
        }
        this.props.CariBarangSetiing(arrbr);

          
    }

    render() {
        return (
            <View style={styles.container}>
                <View state={styles.header}>
                    <TextInput
                        style={{
                             backgroundColor: 'white',
                        marginTop: hp('4%'),
                        width:wp('99%')
                        ,height:hp('7%') 
                        }}
                        placeholder='Cari barang..'
                        onChangeText={(searchText => this.masuk({ searchText }))}
                    />
                 </View>
                 <FlatList 
                    data={this.props.arraybarang}
                    renderItem={(databarang)=> <ListItemsetting updateBarang={this.updateBarang.bind(this)} databarang={databarang.item} getprop={this.props}/>}
                    keyExtractor={(skill)=> (skill.id+'')}
                />
              
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    header: {
        
      },
      
    itemContainer: {
    flexDirection:'row',
    borderWidth:4,
    },
      
})


const mapStateToProp=(state)=>{
   // console.log("imapStateToProp Setting ",state.arraybarang);
    return {
        arraycari:state.arraysearchbarang,
        arraybarang:state.arraybarangsetting,
        namabar:state.namabar,
        hargabar:state.hargabar,
        srokbar:state.srokbar
    }
}
const mapDispatchToProp=(dispatch)=>{
    return{
        SetNAmabar:(a)=> dispatch({type:'Set_Namabar',value:a}),
        SetHargabar:(a)=> dispatch({type:'Set_Hargabar',value:a}),
        SetStokbar:(a)=> dispatch({type:'Set_Stokbar',value:a}),
        SetLiseBarang:(a)=> dispatch({type:'Set_Lisebarang',value:a}),
        CariBarangSetiing:(a)=> dispatch({type:'Cari_BarangSetiing',value:a}),
       // Setpassword:(a)=> dispatch({type:'Setpassword',password:a}),
    }
  
  }





export default connect(mapStateToProp,mapDispatchToProp) (Setting);