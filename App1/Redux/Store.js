import React, { Component } from 'react'
import {createStore,applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import IndukReducer from './Reducer'

const Storeredux =createStore(IndukReducer,applyMiddleware(thunk));
export default Storeredux;

