import React  from 'react';
import { ActivityIndicator,View, Text, TextInput, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {connect} from 'react-redux'
import Home from './Homescreen'



class Login extends React.Component {
    constructor(props) {
        super(props);
               
      }
    loading (data){
        console.log('ini loading ')
        this.props.setloading()
        this.props.UserApi({user:this.props.user,password:this.props.password})
        }

    render() {
        return (
            <View style={{flex: 1}} >
                {this.props.login ?
                    <Home></Home> 
                    :
                    <View style={styles.container}>
                        {this.props.loading ? <ActivityIndicator size="large" style={{alignItems:'center',justifyContent:'center',top:wp('100%')}} /> :
                        <View style={{flex: 1}}>
                        <View style={styles.header}>
                            <Text style={styles.textltittle}>Welcome!</Text>
                        </View>
                        <View style={styles.footer}>
                            <View style={styles.bodygit}>
                            <Image source={require('../assets/logo.jpg')} style={styles.imagelogo} />
                                <Text style={styles.textltittlelog}>LOGIN</Text>
                                <View style={styles.bodygit2}>
                                    <Icon name="user-circle" style={styles.icon1} />
                                    <TextInput style={styles.textinput} 
                                    placeholder='Username / Email' 
                                    defaultValue="ahmad"
                                    onChangeText={text => this.props.Setuser(text)}/>
                                </View>
                                <View style={styles.bodygit2}>
                                    <Icon name="unlock" style={styles.icon2} />
                                    <TextInput style={styles.textinput} 
                                    placeholder='Password'
                                    defaultValue="A"
                                    onChangeText={text => this.props.Setpassword(text)} />
                                </View>
                            </View>

                                <View style={styles.bodygit}>
                                    <View style={styles.tengah}>
                                        <TouchableOpacity style={styles.appButtonContainer}
                                        onPress={this.loading.bind(this)} >
                                            <Text style={styles.appButtonText}>Sign In</Text>
                                        </TouchableOpacity>
                                        
                                    </View>
                                </View>
                            </View>
                            </View>
                            }
                        </View>
                 
                 }
            </View>
                                              
            
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0C50FF',
       // borderWidth:10
        
    },
    header: {
        //flex: 1,
        //borderWidth:5,
        //backgroundColor:'yellow',
        height:hp('25%'),
        width:wp('100%')

        
    },
    footer: {
        //flex: 3,
        flex: 1,
        backgroundColor: '#fff',
       // backgroundColor: 'red',
        borderTopStartRadius: hp('7%'),
        borderTopEndRadius: hp('7%'),
        paddingVertical: hp('7%'),
        paddingHorizontal: wp('7%'),
        borderBottomLeftRadius: hp('7%'),
        borderBottomRightRadius: hp('7%'),
        borderLeftWidth: hp('3%'),
        borderRightWidth: hp('3%'),
        borderRightColor: '#0C50FF',
        borderLeftColor: '#0C50FF',
    },
    textltittle: {
        fontSize: hp('4%'),
        color: '#fff',
        paddingTop: hp('15%'),
        left: wp('7%')
    },
    imagelogo : {
        width: 120,
        height: 30,
        alignSelf: 'flex-end',
        top:hp('-5%')
    },
    textltittlelog: {
        fontSize: hp('7%'),
        textAlign: 'center',
        color: '#1373E9'
    },
    textinput: {
        margin: hp('1%'),
        height: hp('7%'),
        width: wp('68%'),
        borderWidth: 1,
        paddingHorizontal: wp('5%'),
        borderRadius: wp('1%'),
        marginTop: 0
    },
    textltittle2: {
        fontSize: hp('5%'),
        color: '#1373E9',
        textAlign: 'center',
    },
    appButtonContainer: {
        backgroundColor: "#1373E9",
        borderRadius: hp('5%'),
        paddingVertical: hp('2%'),
        width: wp('40%'),
        justifyContent: 'center',
        alignItems: 'center',
    },
    appButtonText: {
        fontSize: hp('3%'),
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center"
    },
    appButtonContainer2: {
        backgroundColor: "#F3101B",
        borderRadius: hp('5%'),
        paddingVertical: hp('2%'),
        width: wp('40%'),
        justifyContent: 'center',
        alignItems: 'center'
    },
    appButtonText2: {
        fontSize: hp('3%'),
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center"
    },
    bodygit: {
        paddingVertical: hp('1%'),
        flexDirection: "column",
    },
    bodygit2: {
        paddingVertical: hp('2%'),
        flexDirection: "row",
    },
    tengah: {
        alignItems: 'center'
    },
    or: {
        paddingTop: hp('2%'),
        paddingBottom: hp('5%'),
        alignItems: 'center'
    },
    textfooter: {
       // paddingTop:  hp('10%'),
       // paddingBottom: 0,
       // alignItems: 'center',
        color: '#e6e600'
    },
    textfooter2: {
        color: '#3333ff',
        fontStyle: 'italic'
    },
    icon1:{
        left:wp('-2%'),
        fontSize:wp('9%')
    },
    icon2:{
        left:wp('-2%'),
        fontSize:wp('9%')
    }
});

const CekUserApi = (data) => (dispatch) =>  {
    console.log('ini dispatch ',data)
    //console.log('ini props  ',this.date)
    let url='https://azd25-cc8c3.firebaseio.com/DB/user/'+data.user+".json";
    fetch(url)
          .then(res => res.json())
          .then(res => {
              if(res.error) {
                  throw(res.error);
              }
              console.log('ininreponyaaaaaaaaaaaaaaaaaaaaa ',res)
              if(res==null){
                alert(' User Tidak Ditemukan ');
                dispatch({type:'Update_User',value:"",respon:false});
              }else if(data.password==res.password){
                dispatch({type:'Update_User',value:res,respon:true});
              }else{
                alert('Password Tidak sesuai ');
                dispatch({type:'Update_User',value:"",respon:false});
              }
                        
              //return res.products;
          })
          .catch(error => {
            alert(' User Tidak Ditemukan ');
            dispatch({type:'Update_User',value:"",respon:false});
          })
  }

const mapStateToProp=(state)=>{
    return {
        user:state.namauser,
        password:state.pasword,
        login:state.login,
        loading:state.loading,
    }
  
  }
  
  
  
  
  const mapDispatchToProp=(dispatch)=>{
    return{
        Setuser:(a)=> dispatch({type:'Setuser',namauser:a}),
        Setpassword:(a)=> dispatch({type:'Setpassword',password:a}),
        setloading:() => dispatch({type:'Update_loading'}),
        UserApi:(a) => dispatch(CekUserApi(a))
    }
  
  }


export default connect(mapStateToProp,mapDispatchToProp)(Login);