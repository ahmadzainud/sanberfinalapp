import React, {Component} from 'react';
import {StyleSheet,View,Text} from 'react-native'
import { StatusBar } from 'expo-status-bar';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import Loginpage from './Pages/Login';
import Home from './Pages/Homescreen';
import {Provider} from 'react-redux'
import Storeredux from './Redux/Store'


class Login extends Component {
  render() {
    return (

      <Provider store={Storeredux}><Loginpage></Loginpage></Provider>

    );
  }
}
 
export default Login;
