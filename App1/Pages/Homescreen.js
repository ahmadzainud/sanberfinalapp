import React  from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import {connect} from 'react-redux'

import Ionicons from 'react-native-vector-icons/FontAwesome5';

import Homepage from './Home';
import Aboutpage from './About';
import Settingpage from './Setting';


const Tab = createBottomTabNavigator();

const Kembalikehome= ()=>{
    console.log('this.propspppppppppppppppppppppppppppppppppppppppppppppp ',this.props);
    return(
        alert('kembali ke home ')
       // this.props.KembaliHome()
    )
}

//


 class Homescreen extends React.Component {
    render() {
        console.log('ini home ',this.props.jenisuser)
        return (
            <NavigationContainer>
            <Tab.Navigator
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ color, size }) => {
                        let iconName;
                        if (route.name === 'Home') {
                            iconName = 'home';
                        } else if (route.name === 'About') {
                            iconName = 'caret-square-right';
                        }else if (route.name === 'Setting') {
                            iconName = 'wrench';
                        }
                        return <Ionicons name={iconName} size={size} color={color} />;
                    }
                })}
                tabBarOptions={{
                    activeTintColor: 'blue',
                    inactiveTintColor: 'tomato',
                }}
            >
                
                <Tab.Screen name="Home" component={Homepage} />
                <Tab.Screen name="About" component={Aboutpage} />
                {this.props.jenisuser==='admin'? 
                <Tab.Screen name="Setting" component={Settingpage} /> :
                <></>
                 }
                 
                
                
                
            </Tab.Navigator>
            </NavigationContainer>
        )
    }
}


const mapStateToProp=(state)=>{
    console.log("imapStateToProp home ",state.namauser);
    return {
        jenisuser:state.user.jenisuser
    }
}
const mapDispatchToProp=(dispatch)=>{
    return{
        KembaliHome:(a)=> dispatch({type:'Kembali_Home'}),
    }
  
  }

export default connect(mapStateToProp,mapDispatchToProp) (Homescreen);

